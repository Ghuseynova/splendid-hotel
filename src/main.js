

(function () {
  const header = document.querySelector('.js-header');
  const burger = document.querySelector('.js-burger');
  const nav = document.querySelector('.js-nav');
  // const navLinks = nav.querySelectorAll('.nav__link');

  function handleBurger() {
    this.classList.toggle('burger--is-open');
    header.classList.toggle('header--is-open');
    nav.classList.toggle('nav--is-open');
  }

  // function handleNavLink() {
  //   navLinks.forEach((navLink) =>
  //     navLink.classList.remove('nav__link--is-active')
  //   );
  //   this.classList.add('nav__link--is-active');
  //   burger.classList.remove('burger--is-opened');
  //   header.classList.remove('header--is-open');
  //   nav.classList.remove('nav--is-fixed');
  // }

  burger.addEventListener('click', handleBurger);

  // navLinks.forEach((navLink) =>
  //   navLink.addEventListener('click', handleNavLink)
  // );
})();


(function() {
  const calendars = bulmaCalendar.attach('[type="date"]', {
    type: "date",
    dateFormat: "d MMM yyyy",
    startDate: new Date(),
    showHeader: false,
    clearLabel: false,
    showFooter: false,
    enableMonthSwitch: false
  });

  calendars.forEach(calendar => {
  
    calendar.on('show', () => {
      console.log('opened', calendar.element.closest('.check-select'));
      calendar.element.closest('.check-select').classList.add('check-select--is-open');
    })

    calendar.on('hide', () => {
    
      calendar.element.closest('.check-select').classList.remove('check-select--is-open');
    })
  })
})();

(function() {

  const selectTriggers = document.querySelectorAll('.js-select-trigger');
  const options = document.querySelectorAll('.js-option');


  selectTriggers.forEach(selectTrigger => {
    selectTrigger.addEventListener('click', function() {

      this.closest('.check-select').classList.add('check-select--is-open');
    })
  });

  options.forEach(optionsElem => {
    optionsElem.addEventListener('click', function(e) {
      const option = e.target;
      const input = option.closest('.check-select').querySelector('.check-select__input');

      optionsElem.querySelectorAll('.check-select__option').forEach(option => {
        option.classList.remove('check-select__option--is-active')
      })

      option.classList.add('check-select__option--is-active');

      input.textContent = option.textContent;
      option.closest('.check-select').classList.remove('check-select--is-open');
    
    })
  })

  
  document.addEventListener('click', function(e) {
    if (!e.target.matches('.js-option, .js-option *, .js-select-trigger, .js-select-trigger *')) {
      options.forEach(optionsElem => {
        optionsElem.closest('.check-select').classList.remove('check-select--is-open');
      })
    }
  })

})();

(function() {
  const rSliders = document.querySelectorAll('.r-slider');

  rSliders.forEach(rSlider => {
    const container = rSlider.querySelector('.r-slider__inner');
   const  controlsContainer=  rSlider.querySelector('.r-slider__controls');

    const slider = tns({
      container,
      controlsContainer,
      items: 1,
      slideBy: 'page',
      nav: true,
      loop: false,
    });

    setTimeout(()=> {
      const tNavs = rSlider.querySelector('.tns-nav');
      const el = document.createElement('div');
      el.className = "r-slider__block";
  
      el.append(tNavs);
      el.append(controlsContainer);
  
      rSlider.append(el)
  
    }, 0)
  
  })
 
})();

(function () {
  const myTabs = document.querySelectorAll('.js-tab');

  if (myTabs.length !== 0) {

    function myTabClicks(tabClickEvent) {
      const clickedTab = tabClickEvent.currentTarget;

      myTabs.forEach(myTab=> {
        myTab.classList.remove('tab-item--is-active');
      })

      clickedTab.classList.add('tab-item--is-active');
    

      const myContentPanes = document.querySelectorAll('.tab-content');


      myContentPanes.forEach(myContentPane => {
        myContentPane.classList.remove('tab-content--is-active');
      })

      const activePaneId = clickedTab.dataset.content;
      const activePane = document.querySelector(`#${activePaneId}`);

      activePane.classList.add('tab-content--is-active');

    }

    myTabs.forEach(myTab=> {
      myTab.addEventListener('click', myTabClicks);
    })
  }
})();

(function() {
  const tSlider = document.querySelector('.js-t-slider');


  if(tSlider) {
    const container = tSlider.querySelector('.t-slider__inner');
    const controlsContainer = tSlider.querySelector('.t-slider__controls');

    const slider = tns({
      container,
      controlsContainer,
      items: 1,
      slideBy: 'page',
      nav: true,
      loop: false,
    });
  }


})();

(function() {
  const filters = document.querySelector('.js-filter');

  if(filters) {
    const filterItems = filters.querySelectorAll('.c-filter__link');

    filterItems.forEach(filterItem => {
      filterItem.addEventListener('click', function() {
       
        const filter = this.dataset.filter;
        const newsItems = document.querySelectorAll('.news-item');
  
        filterItems.forEach(filterItem => filterItem.classList.remove('c-filter__link--is-active'));
        this.classList.add('c-filter__link--is-active');
  
        newsItems.forEach(newsItem => {
          const category = newsItem.dataset.category;
  
          if(category === filter || filter === 'all') {
            newsItem.style.display = ''
          } else {
            newsItem.style.display = 'none'
          }
  
        })
  
      })
    })
  }


})();


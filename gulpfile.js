'use strict';

const gulp = require('gulp');
const del = require('del');
const newer = require('gulp-newer');
const plumber = require('gulp-plumber');
const concat = require('gulp-concat');
const rename = require('gulp-rename');

const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');
const mozjpeg = require('imagemin-mozjpeg');
const svgstore = require('gulp-svgstore');
const webp = require('gulp-webp');

const sass = require('gulp-sass')(require('node-sass'));
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const minify = require('gulp-csso');

const babel = require('gulp-babel');
const include = require('gulp-include');
const uglify = require('gulp-uglify');
const rigger = require('gulp-rigger');

const pug = require('gulp-pug');
const prettier = require('gulp-pretty-html');

const server = require('browser-sync').create();

gulp.task('clean', function () {
  return del('build');
});

gulp.task('copy', function () {
  return gulp
    .src(
      [
        'src/fonts/**/*.{ttf,eot,woff,woff2}',
        'src/images/**/*.{png,jpg,svg}',
        'src/favicons/**',
      ],
      {
        base: 'src',
      }
    )
    .pipe(gulp.dest('build'));
});

gulp.task('images', function () {
  return gulp
    .src(['src/images/*.{png,jpg,svg}', '!src/images/icons/*.svg'])
    .pipe(newer('build/images'))
    .pipe(
      imagemin([
        pngquant({
          speed: 1,
          quality: [0.7, 0.9],
        }),
        imagemin.optipng({
          optimizationLevel: 3,
        }),
        imagemin.mozjpeg({
          progressive: true,
        }),
        mozjpeg({
          quality: 90,
        }),
        imagemin.svgo({
          plugins: [
            {
              removeViewBox: false,
            },
          ],
        }),
      ])
    )
    .pipe(gulp.dest('build/images'));
});

gulp.task('video', function () {
  return gulp.src('src/video/*').pipe(gulp.dest('build/video'));
});

gulp.task('webp', function () {
  return gulp
    .src('build/images/*.{png,jpg}')
    .pipe(webp({ quality: 90 }))
    .pipe(gulp.dest('build/images/webp'));
});

gulp.task('sprite', function () {
  return gulp
    .src('src/images/icons/*.svg')
    .pipe(newer('build/images'))
    .pipe(
      imagemin([
        imagemin.svgo({
          plugins: [
            {
              removeViewBox: false,
            },
          ],
        }),
      ])
    )
    .pipe(
      svgstore({
        inlineSvg: true,
      })
    )
    .pipe(rename('sprite.svg'))
    .pipe(gulp.dest('build/images'));
});

gulp.task('style', function () {
  return gulp
    .src('src/styles/main.scss')
    .pipe(plumber())
    .pipe(
      sass({
        outputStyle: 'expanded',
      })
    )
    .pipe(postcss([autoprefixer()]))
    .pipe(gulp.dest('build/css'))
    .pipe(minify())
    .pipe(rename('style.min.css'))
    .pipe(gulp.dest('build/css'))
    .pipe(server.stream());
});

gulp.task('scripts:vendor', function () {
  const libs = [
    "src/js/libs/bulma-calendar.js",
    "src/js/libs/tiny-slider.js",
  ]

  return gulp
    .src(libs)
    .pipe(concat('vendor.js'))
    .on('error', console.log)
    .pipe(plumber())
    .pipe(rigger())
    .pipe(gulp.dest('build/js'))
    .pipe(uglify())
    .pipe(
      rename({
        suffix: '.min',
      })
    )
    .pipe(gulp.dest('build/js'));
});

gulp.task('scripts', function () {
  return gulp
    .src('src/main.js')
    .pipe(include())
    .on('error', console.log)
    .pipe(
      babel({
        presets: ['@babel/preset-env'],
      })
    )
    .pipe(plumber())
    .pipe(rigger())
    .pipe(gulp.dest('build/js'))
    .pipe(uglify())
    .pipe(
      rename({
        suffix: '.min',
      })
    )
    .pipe(gulp.dest('build/js'));
});

gulp.task('html', function () {
  return gulp
    .src('src/pug/**/*.pug')
    .pipe(pug({
      pretty: true
    }))
    .pipe(plumber())
    .pipe(gulp.dest('build'))
    .pipe(server.stream());
});

gulp.task('refresh', function (done) {
  server.reload();
  done();
});

gulp.task(
  'build',
  gulp.series('clean', 'copy', 'webp', 'sprite', 'style', 'scripts', 'scripts:vendor', 'html')
);

gulp.task(
  'build:minify',
  gulp.series(
    'clean',
    'copy',
    'images',
    'webp',
    'sprite',
    'style',
    'scripts',
    'scripts:vendor',
    'html'
  )
);

gulp.task('serve', function () {
  server.init({
    server: 'build/',
    notify: false,
    open: true,
    cors: true,
    ui: false,
  });

  gulp.watch(
    'src/**/*.{png,jpg,svg}',
    gulp.series('images', 'sprite', 'refresh')
  );
  gulp.watch('src/**/*.scss', gulp.series('style', 'refresh'));
  gulp.watch('src/*.js', gulp.series('scripts', 'refresh'));
  gulp.watch('src/**/*.pug', gulp.series('html', 'refresh'));
});

gulp.task('start', gulp.series('build', 'serve'));
